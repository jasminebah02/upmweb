let etudiant={
    nom: "yahyaoui",
    age: 22,
    direBonjour: function(){
        console.log(`Bonjour! mon nom est ${this.nom} et j'ai ${this.age} ans`)
    }
}

let etudiant2={
    nom: "yahyaoui",
    age: 22,
    direBonjour: function(){
        console.log(`Bonjour! mon nom est ${this.nom} et j'ai ${this.age} ans`)
    }
}

let e=etudiant;
console.log(`e==etudiant=> ${e==etudiant}`)
console.log(`etudiant2==etudiant=> ${etudiant2==etudiant}`)
e.nom="Moradi"
console.log(`e.nom =>  ${e.nom}`)
console.log(`etudiant.nom =>  ${etudiant.nom}`)
//****************************************************** */
//          Creation des objet en js                     //
//****************************************************** */
// Methode 1 : function return Object

// function Etudiant(nom, age){
//     obj={};
//     obj.nom=nom;
//     obj.age=age;
//     obj.direBonjour=function(){
//         console.log(`Bonjour ${this.nom} ${this.age}`)
//     }
//     return obj;
// }
// let e1=Etudiant("Yahyaoui",22)
// let e2=Etudiant("Rochdi",21)
// let E=[];
// for(let i=0;i<4;i++){
//     let nom=prompt("Nom: ");
//     let age=parseInt(prompt("Age: "))
//     let e=Etudiant(nom, age)
//     E.push(e)
// }
// console.log(E)

// Methode 2
// function Etudiant(nom, age){
//     this.nom=nom;
//     this.age=age;
// }
// Etudiant.prototype.direBonjour=function(){
//     console.log(`Bonjour ${this.nom} ${this.age}`)
// }
// let e0=Etudiant("Karam",24)
// let e1=new Etudiant("Yahyaoui",22)
// let e2=new Etudiant("Rochdi",21)
// let E=[];
// for(let i=0;i<2;i++){
//     let nom=prompt("Nom: ");
//     let age=parseInt(prompt("Age: "))
//     let e=new Etudiant(nom, age)
//     E.push(e)
// }
// console.log(E)

//Methode 3 .  class

class Etudiant{
    constructor(nom, age){
        this.nom=nom;
        this.age=age;
    }
    direBonjour(){
        console.log(`Bonjour ${this.nom} ${this.age}`)
    }
}

//let e0=Etudiant("Karam",24)
let e1=new Etudiant("Yahyaoui",22)
let e2=new Etudiant("Rochdi",21)
let E=[];
// for(let i=0;i<2;i++){
//     let nom=prompt("Nom: ");
//     let age=parseInt(prompt("Age: "))
//     let e=new Etudiant(nom, age)
//     E.push(e)
// }
console.log(E)

// L'objet Date
let str=Date()
console.log(str)  // String
let d= new Date()  //objet
console.log(d);
function showTime(){
    let d=new Date();
    console.log(`${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`)
}

let I=setInterval(showTime,1000)
setTimeout(function stopInterval(){
    clearInterval(I)
}, 2000)

//Les tableaux
let T=["un", 3, true]
console.log(T)
T.push("Trois")
console.log(T)
T.unshift(1234)
console.log(T)
let a=T.pop()
console.log(T)
console.log(a)
let b=T.shift()
console.log(T)
console.log(b)