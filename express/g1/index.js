const express=require("express");
const fs=require("fs")
const app=express();
let users=[
    {
        id:1,
        name:"yahya",
        age:21
    },
    {
        id:3,
        name:"Manale",
        age:21
    },
    {
        id:5,
        name:"Rawya",
        age:21
    },
    {
        id:5,
        name:"Moymoun",
        age:21
    },
    {
        id:21,
        name:"Manale",
        age:21
    }
]
app.use(express.static(__dirname+"/public"))

app.use((req,res,next)=>{
    req.date=new Date()
    req.name="gounane"
    console.log(req.name,":" ,req.date,":",req.url)
    next();

})
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.get("/",(req,res)=>{
    fs.readFile(__dirname+"views/index.html","utf-8",(er,data)=>{

    })
    
})

app.post("/",(req,res)=>{
    users.push(req.body)
    res.json(users);
})
app.get("/users",(req, res)=>{
    res.send("users list")
})

app.get("/api/users",(req, res)=>{
    res.json(users)
})
app.get("/api/users/:id",(req, res)=>{
   // console.log(req.params)
    let id=parseInt(req.params.id)
    const r=users.find(elem=>{return id==elem.id})
    console.log(r);
    if(r){
        res.json(r)
    }
    else{
        res.json({status: "USER Not FOUND"})
    }
})




app.use((req,res)=>{
    res.status(404)
    res.send("<h1>404: Page Not Found</h1>")
})

app.listen(3000);