const express=require("express");
const fs=require("fs")
const app=express();

app.set("view engine","pug");
app.set("views","views")

let users=[
    {
        id:1,
        name:"yahya",
        age:21
    },
    {
        id:3,
        name:"Manale",
        age:21
    },
    {
        id:5,
        name:"Rawya",
        age:21
    },
    {
        id:5,
        name:"Moymoun",
        age:21
    },
    {
        id:21,
        name:"Manale",
        age:21
    }
]

app.use(express.static(__dirname+"/public"))

app.use((req,res,next)=>{
    
    req.info={
        url:req.url,
        date: new Date(),
        clientIp: req.ip
    }
    
    next();
})
app.use(express.urlencoded({extended:false}))
app.get("/",(req,res)=>{
    console.log(req.info);
    res.sendFile(__dirname+"/public/index.html")
})
// app.get("/users",(req,res)=>{
//     fs.readFile(__dirname+"/views/users.html","utf-8",(er,data)=>{
//         str="<ol>";
//         users.forEach(e=>{
//             str+=`<li> name : ${e.name}</li><li> age: ${e.age}</li>`
//         })
//         str+="</ol>"
//         data=data.replace("{{list}}",str)
//         res.send(data)
//     })
//     //res.sendFile(__dirname+"/public/users.html")
// })

app.get("/users",(req,res)=>{
    res.render("users",{users:users})
})
app.get("/api/users",(req,res)=>{
    res.json(users)
})
app.get("/api/users/:id",(req,res)=>{
   // console.log(req.params)
   let id=parseInt(req.params.id)
   console.log(id) 
   let r=users.find(elem=> elem.id==id )
   if(r){
    res.json(r)
   }
   else{
    res.json({msg:"User not found"})
   }
   
})
app.post("/api/users",(req,res)=>{
    console.log(req.body.name);
    let id=users[users.length-1].id+1;
    let user=req.body;
    user.id=id;
    users.push(user)
    res.json(users)
})
app.delete("/api/users/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    if(id){
        users=users.filter(elem=>elem.id!=id)
        res.json({msg:`user ${id} deleted`})   
    }
    else{
        res.json({msg:"User not found"})
    }
})
app.put("/api/users/:id",(req,res)=>{
    const id=parseInt(req.params.id);
    if(id){
        let user=users.find(elem=>elem.id==id)
        user.name=req.body.name;
        user.age=req.body.age
        res.json({msg:`user ${id} updated`})   
    }
    else{
        res.json({msg:"User not found"})
    }
})
app.use((req,res)=>{
    res.status(404);
    res.send("<h1>404: page not found</h1>");
})
app.listen(3000);