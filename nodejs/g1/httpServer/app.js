const http=require("http");
const fs=require("fs")
const server=http.createServer((req, res)=>{
    let obj={
        name: "Mohammed",
        age:12,
        notes:[12,16.6,18]
    }

    if(req.url=="/"){
        res.setHeader("Content-type","text/html")
        //res.writeHead(200)
        fs.readFile("./index.html","utf-8",(er,data)=>{
            if(er) {
                res.writeHead(404)
                res.write("ERRROR!!!Ò")
            }
            else{
                res.write(data)
            }
            res.write("Body")
            res.end()
        })
        
       
    }
    else if(req.url=="/style.css"){
        fs.readFile("./style.css","utf-8",(er,data)=>{
            res.end(data)
        })
    }
    else if(req.url=="/api"){
        res.setHeader("Content-type","application/json")
        res.writeHead(200)
        res.write(JSON.stringify(obj))
        res.end()
    }
    else{
        res.setHeader("Content-type","text/html")
        res.writeHead(404)
        res.write(`<h1>404: PAGE NOT FOUND</h1>`)
        res.end()
    }
    
})
server.listen(3000,()=>console.log("visite http://localhost:3000"))
